Drupal 6 doesn't allow you to add a description to a menu link when adding it directly from a node add/edit form. Instead, it uses the link's text as the description, which means that the link's title attribute (i.e., "tooltip") will be the same as the link text itself. This means that if you don't like this behavior, you must manually edit each link after it is created. Boo.

This module fixes that.

Features
- Adds a "Description" text box to the Menu Settings section of the node add/edit form, just like the one on the Edit Menu Item form
- Allows you to leave the description empty and just not output a link tooltip instead of Drupal's default of making the link text into the title attribute
- Batch delete any menu item description that is the same as that item's link text (this can be done using the checkbox on any menu's Edit Menu form)


